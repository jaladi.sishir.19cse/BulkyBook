using BulkyBookWeb.Controllers.Data;
using Microsoft.EntityFrameworkCore;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.
builder.Services.AddControllersWithViews();
// Add DBContext to the services
// DI, of the ApplicationDBContext everywhere in the project
string dbConnectionString = builder.Configuration.GetConnectionString("DefaultConnection");
// passed in an anonymous function
builder.Services.AddDbContext<ApplicationDBContext>(options => options.UseMySql(dbConnectionString,ServerVersion.AutoDetect(dbConnectionString)));
var app = builder.Build();

// Configure the HTTP request pipeline.
if (!app.Environment.IsDevelopment())
{
    app.UseExceptionHandler("/Home/Error");
    // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
    app.UseHsts();
}

app.UseHttpsRedirection();
app.UseStaticFiles();

app.UseRouting();

app.UseAuthorization();

app.MapControllerRoute(
    name: "default",
    pattern: "{controller=Home}/{action=Index}/{id?}");

app.Run();