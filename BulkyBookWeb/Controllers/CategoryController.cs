using BulkyBookWeb.Controllers.Data;
using BulkyBookWeb.Models;
using Microsoft.AspNetCore.Mvc;

namespace BulkyBookWeb.Controllers;

public class CategoryController : Controller
{
    // How do I retrieve the rows in the database through this code
    private readonly ApplicationDBContext _dbContext;
    // Tell the project to recieve the object of ApplicationDBContext to work with the Categories table
    // Whatever registered inside the DI(Dependency Ingestion) container can be accessed as the parameters of the constructor
    public CategoryController(ApplicationDBContext db)
    {
        _dbContext = db;
    }
    // GET
    public IActionResult Index()
    {
        // Get the list of rows from the Category Table
        // all the list of rows can be aggregated to IEnumberable
        IEnumerable<CategoryModel> categoryList = _dbContext.Categories;
        return View(categoryList);
    }

    // GET
    public IActionResult Create()
    {
        return View();
    }
    // POST
    [HttpPost]
    [ValidateAntiForgeryToken]
    public IActionResult Create(CategoryModel obj)
    {
        // need to save the data that is coming from obj
        // check the model is valid or not, passing all the validations that setup in CategoryModel
        // add custom validations
        if (obj.Name == obj.DisplayOrder.ToString())
        {
            ModelState.AddModelError("name","Category Name and Display Order should not be the same");
        }
        if (ModelState.IsValid)
        {
            _dbContext.Categories.Add(obj);
            _dbContext.SaveChanges();
            return RedirectToAction("Index");
        }

        return View(obj);
    }
    
    //GET
    public IActionResult Edit(int? id)
    {
        // here id can be null
            if (id == null || id == 0)
            {
                return NotFound();
            }
            // get the row with the particular primary id
            var categoryRow = _dbContext.Categories.Find(id);
            if (categoryRow == null) { 
                return NotFound();
        }

        return View(categoryRow);// asp-helper tags will automatically display the values
    }
    // POST
    [HttpPost]
    [ValidateAntiForgeryToken]
    public IActionResult Edit(CategoryModel obj)
    {
        // need to save the data that is coming from obj
        // check the model is valid or not, passing all the validations that setup in CategoryModel
        // add custom validations
        if (obj.Name == obj.DisplayOrder.ToString())
        {
            ModelState.AddModelError("name","Category Name and Display Order should not be the same");
        }
        if (ModelState.IsValid)
        {
            _dbContext.Categories.Update(obj);
            _dbContext.SaveChanges();
            return RedirectToAction("Index");
        }

        return View(obj);
    }
}